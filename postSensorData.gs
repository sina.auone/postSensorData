var book = SpreadsheetApp.getActiveSpreadsheet();

//PostされたJSONデータを受け取り
function doPost(e){

  let parameter = JSON.parse(e.postData.getDataAsString());

  var data = [      
    parameter.Date_Master, // マスター日時     
    parameter.Date, // 測定日時    
    parameter.Temperature, // 気温    
    parameter.Humidity, // 湿度    
    parameter.Light, // 照度    
    parameter.UV, // UV    
    parameter.Pressure, // 圧力    
    parameter.Noise, // 騒音    
    parameter.BatteryVoltage, // 電池電圧    
    new Date(), // アップロード終了時刻    
  ];
  //取得したデータをログに記載
  Logger.log(new Date());
  //スプレッドシートへのデータ行書き込み
  addData(parameter.DeviceName, data);

  //返り値
  let output = ContentService.createTextOutput();
  output.setMimeType(ContentService.MimeType.JSON);
  output.setContent(JSON.stringify({ message: "success!" }));
  return output;
}

//スプレッドシートへのデータ行書き込み
function addData(sheetName, data){
  var sheet = book.getSheetByName(sheetName);
  sheet.appendRow(data);


  let sheet2Data = book.getSheetByName("latest");
  for(let i = 0 ; i < data.length ; i++ ){
    sheet2Data.getRange(2,i+1).setValue(data[i]);
  }  

}
